import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hwg_glass/consts.dart';
import 'package:hwg_glass/utils.dart';

class VideoListing extends StatefulWidget {
  List<String> listGtinFilter;

  VideoListing({this.listGtinFilter = const []});

  @override
  State<StatefulWidget> createState() {
    return _VideoListingState();
  }
}

class _VideoListingState extends State<VideoListing> {
  List<String> fileNames = [];
  int selectedIndex = 0;
  int newSelectedIndex = -1;

  double dragHorizontalStartPosition = 0;

  double dragVerticalStartPosition = 0;
  double dragVerticalEndPosition = 0;

  ScrollController scrollCtrl;

  List<String> directories = [MOVIE_FOLDER];

  @override
  void initState() {
    super.initState();
    fetchDirectoryEntries();
    scrollCtrl = ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          ListView.builder(
            controller: this.scrollCtrl,
            padding: EdgeInsets.only(left: 10, right: 10, top: 10),
            itemCount: fileNames.length,
            itemBuilder: (_, int index) {
              return Container(
                child: Text(
                  fileNames[index].split("/").last,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: newSelectedIndex == index
                        ? Colors.white
                        : selectedIndex == index
                            ? Colors.black
                            : Colors.white,
                  ),
                ),
                padding: EdgeInsets.only(top: 20, bottom: 20),
                color: newSelectedIndex == index
                    ? COLOR_RED
                    : selectedIndex == index
                        ? COLOR_YELLOW
                        : COLOR_BLUE,
              );
            },
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onHorizontalDragStart: (details) {
                dragHorizontalStartPosition = details.globalPosition.dx;
              },
              onHorizontalDragUpdate: (details) {
                var distance = details.globalPosition.dx - this.dragHorizontalStartPosition;

                int newCalculatedIndex = selectedIndex - (distance / 50).floor();
                if (newCalculatedIndex < 0) {
                  newCalculatedIndex = 0;
                } else if (newCalculatedIndex > fileNames.length - 1) {
                  newCalculatedIndex = fileNames.length - 1;
                }

                if (newCalculatedIndex != newSelectedIndex) {
                  newSelectedIndex = newCalculatedIndex;
                  this.scrollCtrl.animateTo(
                        this.newSelectedIndex * 38.0,
                        duration: Duration(milliseconds: 350),
                        curve: Curves.linear,
                      );
                  // this.scrollCtrl.jumpTo(newSelectedIndex * 38.0);
                  setState(() {});
                }
              },
              onHorizontalDragEnd: (details) {
                selectedIndex = newSelectedIndex;
                newSelectedIndex = -1;
                setState(() {});
              },
              onVerticalDragStart: (details) {
                this.dragVerticalStartPosition = details.globalPosition.dy;
                // debugPrint("$dragVerticalStartPosition");
              },
              onVerticalDragUpdate: (details) {
                this.dragVerticalEndPosition = details.globalPosition.dy;
                // debugPrint("$dragVerticalEndPosition");
              },
              onVerticalDragEnd: (_) {
                // debugPrint((this.dragVerticalStartPosition - this.dragVerticalEndPosition).toString());
                if ((this.dragVerticalStartPosition - this.dragVerticalEndPosition) < -80) {
                  Navigator.pop(context);
                }
              },
              onTap: () {
                if (fileNames[selectedIndex].endsWith("..")) {
                  directories.removeLast();
                  fetchDirectoryEntries();
                } else if (Utils.isDirectory(fileNames[selectedIndex])) {
                  directories.add(fileNames[selectedIndex]);
                  fetchDirectoryEntries();
                } else {
                  Navigator.pop(context, fileNames[selectedIndex]);
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  List<String> fetchDirectoryEntries() {
    selectedIndex = 0;
    if (this.scrollCtrl != null) {
      this.scrollCtrl.animateTo(0, duration: Duration(milliseconds: 1), curve: Curves.linear);
    }
    var dir = Directory(directories.last);
    List<String> entries = [];
    if (directories.length > 1) {
      entries.add("..");
    }
    dir.list(followLinks: true, recursive: false).listen((FileSystemEntity entity) {
      if (directories.length == 1 && this.widget.listGtinFilter.length > 0) {
        bool found = false;
        for (int i = 0; i < this.widget.listGtinFilter.length && !found; i++) {
          found = entity.path.indexOf(this.widget.listGtinFilter[i]) >= 0;
          if (found) {
            entries.add(entity.path);
          }
        }
      } else {
        entries.add(entity.path);
      }
      setState(() {
        fileNames = [];
        fileNames.addAll(entries);
      });
    });
  }
}
