import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hwg_glass/data/task.dart';
import 'package:hwg_glass/data/task_material.dart';
import 'package:hwg_glass/utils.dart';
import 'package:hwg_glass/video_listing.dart';
import 'package:hwg_glass/video_player.dart';
import 'package:hwg_glass/view/pdf_view.dart';

class TaskDetailView extends StatefulWidget {
  Task task;

  TaskDetailView(this.task);

  @override
  State<StatefulWidget> createState() => _TaskDetailViewState();
}

class _TaskDetailViewState extends State<TaskDetailView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Detail Ansicht - langer Tap für Zurück")),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          showFileListing();
        },
        onLongPress: () {
          Navigator.of(context).pop();
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: ListView(
            children: [
              SizedBox(height: 10),
              getBoldText('Zeit:'),
              getText(Utils.myDTFormat(this.widget.task.startTime, this.widget.task.duration)),
              SizedBox(height: 10),
              getBoldText('Aufgabe:'),
              getText(this.widget.task.todo),
              SizedBox(height: 10),
              getBoldText('Kunde'),
              getText(this.widget.task.client.name),
              getText(this.widget.task.client.address),
              getText(this.widget.task.client.phone),
              getText(this.widget.task.client.email),
              SizedBox(height: 10),
              getBoldText('Element(e) - Tap für Dateiansicht'),
              getMaterials(this.widget.task.listTaskMaterial),
            ],
          ),
        ),
      ),
    );
  }

  void showFileListing() async {
    List<String> gtinList = this.widget.task.listTaskMaterial.map((e) => e.gtin).toList();
    final result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => VideoListing(
              listGtinFilter: gtinList,
            )));
    if ((result ?? "").length > 0) {
      if ((result as String).toLowerCase().endsWith(".pdf")) {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => PDFViewer(result)));
      } else {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => VideoPlayer(result, 0, isLocalFile: true)));
      }
    }
  }

  Widget getBoldText(String text) {
    return Text(text,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ));
  }

  Widget getText(String text, {bool bold = false}) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 16,
        fontWeight: bold ? FontWeight.bold : FontWeight.normal,
      ),
    );
  }

  Widget getMaterials(List<TaskMaterial> ltm) {
    List<Widget> lw = [];
    for (var m in ltm) {
      lw.add(getText(m.manufacturer, bold: true));
      lw.add(getText(m.description));
      lw.add(getText(m.gtin));
      lw.add(SizedBox(height: 10));
    }
    return Column(
      children: lw,
      crossAxisAlignment: CrossAxisAlignment.start,
    );
  }
}
