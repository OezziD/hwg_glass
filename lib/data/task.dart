import 'dart:convert';

import 'package:enough_mail/enough_mail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hwg_glass/data/client.dart';
import 'package:hwg_glass/data/email.dart';
import 'package:hwg_glass/data/task_material.dart';
import 'package:intl/intl.dart';

class Task {
  String id = "";
  String todo = "";
  int duration = 0;
  DateTime startTime;

  DateTime emailSendDT;

  Client client;
  List<TaskMaterial> listTaskMaterial = [];

  String jsonString = "";

  //fetch emails with jobs for today!
  //save them for today

  static Future<List<Task>> fetchTasksFromInbox(DateTime dt) async {
    Map<String, Task> mapOfTasks = {};
    Email emailCredentials = Email();
    await emailCredentials.fetchEmailCredentials();
    final client = ImapClient(isLogEnabled: false);
    try {
      await client.connectToServer(emailCredentials.imap, int.parse(emailCredentials.imapPort), isSecure: true);
      await client.login(emailCredentials.email, emailCredentials.password);
      final mailboxes = await client.listMailboxes();
      await client.selectInbox();
      SearchImapResult sir = await client.searchMessagesWithQuery(
          SearchQueryBuilder.from("[${dt.year}.${dt.month}.${dt.day}]", SearchQueryType.subject));
      for (var id in sir.matchingSequence.toList()) {
        FetchImapResult fir = await client.fetchMessage(id, "BODY[]");
        String str = fir.messages[0].parts[0].decodeContentText().replaceAll("\r", "").replaceAll("\n", "");
        debugPrint(str);
        Task newTask = Task.parseTaskJson(str);
        newTask.emailSendDT = getEmailSendDateTimeFromHeaders(fir.messages[0].headers);

        if (mapOfTasks.containsKey(newTask.id)) {
          Task alreadyExistingTask = mapOfTasks[newTask.id];

          if (alreadyExistingTask.emailSendDT.compareTo(newTask.emailSendDT) < 0) {
            mapOfTasks[newTask.id] = newTask;
          }
        } else {
          mapOfTasks[newTask.id] = newTask;
        }
      }
      // FetchImapResult fir = await client.fetchMessages(sir.matchingSequence, "BODY[]");
      // for (MimeMessage message in fir.messages){
      //   debugPrint(message.parts[0].decodeContentText());
      // }
      // final fetchResult = await client.fetchRecentMessages(messageCount: 10, criteria: 'BODY.PEEK[]');

      await client.logout();
    } on ImapException catch (e) {
      print('IMAP failed with $e');
    }
    //sort by time
    List<Task> listOfTasks = mapOfTasks.values.toList();
    listOfTasks.sort((Task a, Task b) {
      return a.startTime.compareTo(b.startTime);
    });
    return listOfTasks;
  }

  static Task parseTaskJson(String jsonString) {
    dynamic d = JsonDecoder().convert(jsonString);
    Task t = Task();
    t.jsonString = jsonString;

    return fromJson(t, d);
  }

  static Task fromJson(Task t, dynamic d) {
    t.id = d['id'];
    t.startTime = DateTime.parse(d['startTime']);
    t.duration = int.parse(d['duration']);
    t.todo = d['todo'];

    t.client = Client.createFromJson(d['client']);

    if (d['hits'] != null) {
      for (dynamic hit in d['hits']) {
        t.listTaskMaterial.add(TaskMaterial.createFromJson(hit));
      }
    }

    return t;
  }

  static DateTime getEmailSendDateTimeFromHeaders(List<Header> headers) {
    for (Header h in headers) {
      if (h.lowerCaseName == "date") {
        DateTime dt = DateFormat('E, d MMM yyyy HH:mm:ss Z', 'en_US').parse(h.value);
        // Wed, 18 Aug 2021 01:02:50 +0200
        debugPrint(dt.toString());
        return dt;
      }
    }
    return null;
  }
}
