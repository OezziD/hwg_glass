import 'package:hwg_glass/open_data_pool/http_authorization.dart';
import 'package:hwg_glass/open_data_pool/requests.dart';
import 'package:hwg_glass/utils.dart';
import 'package:swagger/api.dart';

class TaskMaterial {
  String gtin = "";
  List<String> imageUrl = [];
  String manufacturer = "";
  String description = "";

  static TaskMaterial createFromJson(dynamic hit) {
    TaskMaterial tm = TaskMaterial();
    tm.gtin = hit['gtin'];
    tm.manufacturer = hit['manufacturer'];
    tm.description = hit['description'];

    if (hit['imageUrl'] != null) {
      tm.imageUrl.add(hit['imageUrl']);
    }
    if (hit['imageUrl2'] != null) {
      tm.imageUrl.add(hit['imageUrl2']);
    }
    downloadGtinDocuments(tm.gtin);
    return tm;
  }

  static List<String> acceptedTypes = ["VI", "VM", "DB", "IS", "MA", "MZ"];

  static downloadGtinDocuments(String gtin) async {
    String accessToken = await MyHttpAuthorization.access_token;
    Product product = await Requests.getProductByGTIN(GTIN(gtin), accessToken);
    String name = product.descriptions.shorttext1;
    var list = product.documents.listDocuments;
    for (int i = 0; i < list.length; i++) {
      if (acceptedTypes.contains(list[i].type.value.toUpperCase())) {
        await Utils.downloadMovieFromOpenDataPool(list[i].url, gtin, name, list[i].filename, accessToken);
      }
    }
  }
}
