import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hwg_glass/consts.dart';
import 'package:hwg_glass/open_data_pool/requests.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:youtube_explode_dart/youtube_explode_dart.dart';

class Utils {
  static String userName = "";

  static void handleJSONType(String jsonData) {
    LinkedHashMap jsonObject = json.decode(jsonData);
  }

  static findMovieFile(String startingWith) {}

  /**
   * Downloads movies from youtube to the local storage const.MOVIE_FOLDER
   */
  static Future<String> downloadMovie(String url) async {
    RegExpMatch match = RegExp(r'v=([^&]+)').firstMatch(url);
    String videoID = match.group(1);

    final dir = Directory(MOVIE_FOLDER);
    final List<FileSystemEntity> files = dir.listSync().toList();
    for (var f in files) {
      if (f.path.split("/").last.startsWith(videoID)) {
        return f.path;
      }
    }

    var yt = YoutubeExplode();
    var video = await yt.videos.get(url);
    var manifest = await yt.videos.streamsClient.getManifest(video.id.value);
    var title = video.title;

    final fileNameTitle = title.replaceAllMapped(RegExp(r'[^a-zA-Z0-9]'), (match) {
      return '_';
    });

    var filePath = "${MOVIE_FOLDER}/${video.id.value}__$fileNameTitle.mp4";
    if (File(filePath).existsSync()) {
      return filePath;
    }

    MuxedStreamInfo muxedStream = null;

    for (var muxedStreamInfo in manifest.muxed) {
      if (muxedStreamInfo.videoQualityLabel == "360p") {
        muxedStream = muxedStreamInfo;
        break;
      }
    }

    if (muxedStream == null) {
      return "";
    }

    var stream = yt.videos.streamsClient.get(muxedStream);

    var file = File(filePath);
    var fileStream = file.openWrite();

    await stream.pipe(fileStream);
    await fileStream.flush();
    await fileStream.close();

    yt.close();
    return filePath;
  }

  static Future<String> downloadMovieFromOpenDataPool(
      String url, String gtin, String name, String filename, String accessToken) async {

    if (await Permission.storage.request().isDenied){
      return "";
    }

    if (await Permission.manageExternalStorage.request().isDenied){
      return "";
    }

    final dir = Directory(MOVIE_FOLDER);
    name = name.replaceAll("ß", "ss").replaceAll(RegExp(r"\s"), "_").replaceAll(RegExp(r"\W"), "");
    Directory dirGtin = Directory(MOVIE_FOLDER + '/' + name.substring(0, 19) + "_" + gtin);
    //final List<FileSystemEntity> files = dir.listSync().toList();

    if (!dirGtin.existsSync()) {
      dirGtin.create();
    }

    var file = File(dirGtin.path + '/' + filename);

    if (file.existsSync()) {
      return file.path;
    }

    Uint8List raw = await Requests.getRawData(url, accessToken);
    file.writeAsBytesSync(raw, flush: true);

    return file.path;
  }

  static bool isDirectory(String path) {
    return Directory(path).existsSync();
  }

  static String myDTFormat(DateTime dt, int duration) {
    return DateFormat("HH:mm").format(dt) + " - " + DateFormat("HH:mm").format(dt.add(Duration(minutes: duration)));
  }
}

Widget getIconWithShadow(IconData iconData, Color color, double size) {
  return Stack(
    children: [
      Positioned(
        left: 4,
        top: 4,
        child: Icon(
          iconData,
          color: COLOR_SHADOW,
          size: size,
        ),
      ),
      Icon(
        iconData,
        size: size,
        color: color,
      ),
    ],
  );
}

OverlayEntry _currentThrobber = null;

OverlayEntry _getThrobber(context) {
  return OverlayEntry(
      builder: (context) => Container(
            color: Color.fromARGB(200, 0, 0, 0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(
                    width: 20,
                    height: 20,
                  ),
                  Text(
                    "Downloading...",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      decoration: TextDecoration.none,
                    ),
                  )
                ]),
          ));
}

int throbberCount = 0;

void showThrobber(context) {
  if (_currentThrobber == null) {
    _currentThrobber = _getThrobber(context);
    Overlay.of(context).insert(_currentThrobber);
  }
  throbberCount++;
}

void removeThrobber() {
  if (_currentThrobber != null && throbberCount == 1) {
    _currentThrobber.remove();
    _currentThrobber = null;
  }
  throbberCount--;
}
