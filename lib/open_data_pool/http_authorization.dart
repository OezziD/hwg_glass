import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class MyHttpAuthorization {
  /*
   * curl --location --request POST 'https://portal.open-datapool.de/oauth2/token' \
      --form 'grant_type="password"' \
      --form 'client_id="248916d0-6555-11eb-a3f7-d35f11582212"' \
      --form 'username="ozguc42621-DIV"' \
      --form 'password="em1er3de"' \
      --form 'scope="openMasterdata"'
   */

  /*
   * Response Data
   * 0 = {map entry} "access_token" -> "687de8e37e27dcf0f75a0f11442dd05c0f81e144"
   * 1 = {map entry} "expires_in" -> 3600
   * 2 = {map entry} "token_type" -> "Bearer"
   * 3 = {map entry} "scope" -> "openMasterdata"
   * 4 = {map entry} "refresh_token" -> "773bb75b76f6424d8cae64368aeabd2ce14a6f8f"
   */

  static String _access_token = "";
  static String _token_type = "";
  static String _scope = "";
  static String _refresh_token = "";

  static Future<String> get access_token async {
    if (_access_token.isEmpty) {
      await fetchToken();
      if (_access_token.isEmpty) {
        return "";
      }
    }

    return _access_token;
  }

  static String getCurrentToken() {
    return _access_token;
  }

  static get token_type => _token_type;

  static get scope => _scope;

  static get refresh_token => _refresh_token;

  static Future<bool> fetchToken() async {
    FlutterSecureStorage secureStorage = FlutterSecureStorage();
    Map<String, String> credentials = await secureStorage.readAll();
    FormData formData = new FormData.fromMap({
      "grant_type": "password",
      "client_id": "248916d0-6555-11eb-a3f7-d35f11582212",
      "username": "oedtiller@gmail.com",
      // "username": credentials["name"],
      "password": "TvVGPGR3BqEw5GW",
      // "password": credentials["password"],
      "scope": "openMasterdata",
    });

    Response response = await Dio().post(
      "https://portal.open-datapool.de/oauth2/token",
      data: formData,
    );

    if (response.statusCode == 200) {
      if (response.data?.length > 0) {
        _access_token = response.data["access_token"];
        _token_type = response.data["token_type"];
        _scope = response.data["scope"];
        _refresh_token = response.data["refresh_token"];
      }
    } else {
      //TODO Something went wrong with my request
    }
    return true;
  }

  static String refreshToken() {
    _access_token = "";
    return MyHttpAuthorization._access_token;
  }


}
