import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as imgLib;
import 'package:swagger/api.dart';

import 'http_authorization.dart';

typedef finalCallback = void Function(bool error);

class Requests {
  static Future<Product> getProductByManufacturerData(
    ManufacturerID id,
    ManufacturerPID pid,
    List<String> dataPackage,
    ManufacturerIDType idType,
    String authorizationToken, {
    int retries = 0,
  }) async {
    Dio dio = Dio();

    Options opt = Options();
    opt.headers = {"Authorization": "Bearer $authorizationToken"};
    opt.responseType = ResponseType.plain;

    var response = await dio.get(
      "https://portal.open-datapool.de/webservicehub/openmasterdata/v1.0.5/product/byManufacturerData",
      queryParameters: {
        "manufacturerId": id.value,
        "manufacturerPid": pid.value,
        "datapackage": dataPackage.join("|"),
        "manufacturerIdType": idType.value,
      },
      options: opt,
    );

    if (response == null) {
      if (retries == 0) {
        MyHttpAuthorization.refreshToken();
        debugPrint("Trying to refresh the token");
        return getProductByManufacturerData(id, pid, dataPackage, idType, await MyHttpAuthorization.access_token,
            retries: 1);
      }
    }

    ApiClient apiClient = ApiClient();
    Product product = apiClient.deserialize(response.data.toString(), 'Product') as Product;
    product.setRawData(response.data.toString());

    return product;
  }

  static Future<Product> getProductByGTIN(
    GTIN gtin,
    String authorizationToken, {
    int retries = 0,
    String dataPackage = 'basic|additional|prices|descriptions|logistics|sparepartlists|pictures|documents',
  }) async {
    Dio dio = Dio();

    Options opt = Options();
    opt.headers = {"Authorization": "Bearer $authorizationToken"};
    opt.responseType = ResponseType.plain;

    debugPrint("now sending the request");

    var response;

    response = await dio.get(
      "https://portal.open-datapool.de/webservicehub/openmasterdata/v1.0.5/product/byGTIN",
      queryParameters: {
        "gtin": gtin.value,
        "datapackage": dataPackage,
      },
      options: opt,
    );

    if (response == null) {
      if (retries == 0) {
        MyHttpAuthorization.refreshToken();
        debugPrint("Trying to refresh the token");
        return getProductByGTIN(gtin, await MyHttpAuthorization.access_token, retries: 1, dataPackage: dataPackage);
      }
    }

    ApiClient apiClient = ApiClient();
    Product product = apiClient.deserialize(response.data.toString(), 'Product') as Product;
    product.setRawData(response.data.toString());

    return product;
  }

  static Future<Product> getProductBySupplierPID(
    SupplierPID supplierPID,
    List<String> dataPackage,
    String authorizationToken,
  ) async {
    Dio dio = Dio();

    Options opt = Options();
    opt.headers = {"Authorization": "Bearer $authorizationToken"};
    opt.responseType = ResponseType.plain;

    var response = await dio.get(
      "https://portal.open-datapool.de/webservicehub/openmasterdata/v1.0.5/product/byGTIN",
      queryParameters: {
        "supplierPid": supplierPID.value,
        "datapackage": dataPackage.join("|"),
      },
      options: opt,
    );

    ApiClient apiClient = ApiClient();
    Product product = apiClient.deserialize(response.data.toString(), 'Product') as Product;
    product.setRawData(response.data.toString());

    return product;
  }

  static Future<Image> getImage(
    String url,
    String authorizationToken, {
    double width,
    int retries = 0,
  }) async {
    if (url == null) {
      //no url is given and the application has still to work
      return Future.value();
    }
    if (authorizationToken.isEmpty) {
      authorizationToken = await MyHttpAuthorization.access_token;
    }

    Dio dio = Dio();
    Options opt = Options();

    opt.headers = {"Authorization": "Bearer $authorizationToken"};

    opt.responseType = ResponseType.bytes;
    var response;
    try {
      response = await dio.get(
        url,
        options: opt,
      );
    } catch (e) {
      debugPrint(e.tostring());
    }

    if (response?.statusCode == 401 && retries == 0) {
      MyHttpAuthorization.refresh_token();
      if (width != null) {
        return getImage(url, await MyHttpAuthorization.access_token, retries: 1, width: width);
      } else {
        return getImage(url, await MyHttpAuthorization.access_token, retries: 1);
      }
    }

    //TIFF SUPPORT
    var data = response.data;
    imgLib.Decoder dec = imgLib.findDecoderForData(response.data);
    if (dec != null && dec.runtimeType == imgLib.TiffDecoder) {
      data = imgLib.encodeJpg(dec.decodeImage(response.data));
    }

    if (width == null) {
      return Image.memory(data);
    } else {
      return Image.memory(
        data,
        width: width,
        height: width,
      );
    }
  }

  static void saveFile(
    String url,
    String absolutePath,
    String authorizationToken,
    ProgressCallback progressCallback,
    finalCallback finalCallback,
  ) {
    Dio dio = Dio();
    Options opt = Options();

    opt.headers = {"Authorization": "Bearer $authorizationToken"};

    opt.responseType = ResponseType.bytes;
    dio.download(url, absolutePath, options: opt, onReceiveProgress: progressCallback).then((value) {
      finalCallback(false);
    });
  }

  static Future<Uint8List> getRawData(
    String url,
    String authorizationToken, {
    int retries = 0,
  }) async {
    Dio dio = Dio();
    Options opt = Options();
    opt.headers = {"Authorization": "Bearer $authorizationToken"};
    opt.responseType = ResponseType.bytes;
    var response = await dio.get(
      url,
      options: opt,
    );

    if (response.statusCode == 401 && retries == 0) {
      MyHttpAuthorization.refresh_token();
      return getRawData(url, await MyHttpAuthorization.access_token, retries: 1);
    }

    return response.data;
  }
}
