# HWG GLASS PROJECT

- [HWG GLASS PROJECT](#hwg-glass-project)
  - [Debugging the HWG Glass](#debugging-the-hwg-glass)
  - [Deleting the data on the glass](#deleting-the-data-on-the-glass)
  - [Touch Gestures in the Code](#touch-gestures-in-the-code)
  - [For Building a release version](#for-building-a-release-version)
  - [Helpful Tool](#helpful-tool)
  - [TODOs](#todos)

## Debugging the HWG Glass
Please have a look at [Android Commands and Debugging](https://tillerstack.atlassian.net/wiki/spaces/~120088074/pages/1508278275/Android+Google+Glass+Commands+and+Utilities) on our confluence page

## Deleting the data on the glass
With the following command you can delete the data of the hwg glass application on the android device
`adb shell pm clear com.tillerstack.hwg_glass`

## Touch Gestures in the Code
I have used GestureDetector Widget for fetching the inputs on the touchpanel of the google glass.

Sometimes it was useful to put the gesturedetector as the last element of the widget tree to retrieve the touch gesture.
e.g. task_view.dart

also using the behavior argument on the gesturedetector was helping.
e.g. opaque, translucent etc...


## For Building a release version
`flutter build apk --release --target-platform android-arm64`

There are changes for the release version so that the VLC Player works
```
buildTypes {
    release {
        minifyEnabled true
        useProguard true
        proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'),'proguard-rules.pro'
        // TODO: Add your own signing config for the release build.
        // Signing with the debug keys for now, so `flutter run --release` works.
        signingConfig signingConfigs.debug
    }
}
```
## Helpful Tool
Please have a look at [ADB tools](https://bitbucket.org/OezziD/adb_tools/src/master/) in the bitbucket repositories

## TODOs
- [ ] same behaviour for the gestures swipe down for leaving and long press
- [ ] cancel button while doing a remote request
- [ ] timeout if something fails during remote request
- [ ] error handling and alert screen
- [ ] change the ui also for mobile phone so that it is also usable there
- [ ] adding bluetooth controller for controlling the app